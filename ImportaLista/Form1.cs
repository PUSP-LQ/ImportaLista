﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ImportaLista
{
    public partial class Form1 : Form
    {
        string connectionString = @"Data Source=066-006822\SQLEXPRESS;Initial Catalog=Votacoa;Integrated Security=True";

        //string connectionString = System.Configuration.ConfigurationSettings.AppSettings["conn"];

        public Form1()
        {
            InitializeComponent();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textBoxPath.Text = openFileDialog1.FileName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void buttonImportar_Click(object sender, EventArgs e)
        {
            if (!(radioButtonCENA.Checked || radioButtonPUSPLQ.Checked || radioButtonESALQ.Checked))
            {
                MessageBox.Show("Selecione a unidade");
                return;
            }

            if (!(radioButtonDocente.Checked || radioButtonAdministrativo.Checked))
            {
                MessageBox.Show("Selecione o funcionário");
                return;
            }

            if (string.IsNullOrEmpty(textBoxOrigem.Text))
            {
                MessageBox.Show("Descreva a origem");
                return;
            }


            string unidade = "ESALQ";
            if (radioButtonCENA.Checked)
            {
                unidade = "CENA";
            }
            else if (radioButtonPUSPLQ.Checked)
            {
                unidade = "PUSP-LQ";
            }

            string funcionario = "Docente";

            if (radioButtonAdministrativo.Checked)
            {
                funcionario = "Administrativo";
            }

            string[] linhas = { };
            try
            {
                linhas = System.IO.File.ReadAllLines(textBoxPath.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problemas: " + ex.Message);
                return;
            }

            string sql = "insert into Funcionarios (NumeroUSP, Nome, Regime, Email, OrigemLista, Unidade, Funcionario) values (@NumeroUSP, @Nome, @Regime, @Email, @OrigemLista, @Unidade, @Funcionario)";
            textBoxSaida.Clear();
            textBoxSaida.Text = "Inserindo...";

            int i = 1;

            toolStripStatusLabel1.Text = "Iniciando....";
            
            foreach (string linha in linhas)
            {
                string[] conteudo = linha.Split(',');

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand();

                    cmd.Parameters.Add(new SqlParameter("@NumeroUSP", conteudo[0]));
                    cmd.Parameters.Add(new SqlParameter("@Nome", conteudo[1]));
                    cmd.Parameters.Add(new SqlParameter("@Regime", conteudo[2]));
                    cmd.Parameters.Add(new SqlParameter("@Email", conteudo[3]));
                    cmd.Parameters.Add(new SqlParameter("@OrigemLista", textBoxOrigem.Text));
                    cmd.Parameters.Add(new SqlParameter("@Unidade", unidade));
                    //cmd.Parameters.Add(new SqlParameter("@Unidade", conteudo[4]));
                    cmd.Parameters.Add(new SqlParameter("@Funcionario", funcionario));


                    cmd.CommandText = sql;
                    cmd.Connection = conn;

                    cmd.ExecuteScalar();

                    textBoxSaida.Text += " \r\n " + i.ToString () + " | " + conteudo[0] + " - " +  conteudo[1] + " - " + conteudo[2];
                    
                    i++;

                    toolStripProgressBar1.Step = i;
                } 
            }

            textBoxSaida.Text += " \r\n Fim.";

            toolStripStatusLabel1.Text = "Finalizado";

        }

        private void enviarMensagemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        
    }
}
