﻿namespace ImportaLista
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.iniciarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxSaida = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxOrigem = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioButtonAdministrativo = new System.Windows.Forms.RadioButton();
            this.radioButtonDocente = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButtonPUSPLQ = new System.Windows.Forms.RadioButton();
            this.radioButtonCENA = new System.Windows.Forms.RadioButton();
            this.radioButtonESALQ = new System.Windows.Forms.RadioButton();
            this.buttonImportar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.textBoxPath = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.enviarMensagemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iniciarToolStripMenuItem,
            this.ajudaToolStripMenuItem,
            this.versãoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(788, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // iniciarToolStripMenuItem
            // 
            this.iniciarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enviarMensagemToolStripMenuItem});
            this.iniciarToolStripMenuItem.Name = "iniciarToolStripMenuItem";
            this.iniciarToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.iniciarToolStripMenuItem.Text = "Iniciar";
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.ajudaToolStripMenuItem.Text = "Ajuda";
            // 
            // versãoToolStripMenuItem
            // 
            this.versãoToolStripMenuItem.Name = "versãoToolStripMenuItem";
            this.versãoToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.versãoToolStripMenuItem.Text = "Versão";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 455);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(43, 17);
            this.toolStripStatusLabel1.Text = "Pronto";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxSaida);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(0, 222);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(788, 233);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Saída";
            // 
            // textBoxSaida
            // 
            this.textBoxSaida.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSaida.Location = new System.Drawing.Point(3, 16);
            this.textBoxSaida.Multiline = true;
            this.textBoxSaida.Name = "textBoxSaida";
            this.textBoxSaida.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxSaida.Size = new System.Drawing.Size(782, 214);
            this.textBoxSaida.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxOrigem);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.buttonImportar);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.buttonOpen);
            this.groupBox1.Controls.Add(this.textBoxPath);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(788, 197);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Importar Dados";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Origem lista:";
            // 
            // textBoxOrigem
            // 
            this.textBoxOrigem.Location = new System.Drawing.Point(81, 52);
            this.textBoxOrigem.Name = "textBoxOrigem";
            this.textBoxOrigem.Size = new System.Drawing.Size(273, 20);
            this.textBoxOrigem.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioButtonAdministrativo);
            this.groupBox5.Controls.Add(this.radioButtonDocente);
            this.groupBox5.Location = new System.Drawing.Point(240, 82);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(195, 109);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Funcionário";
            // 
            // radioButtonAdministrativo
            // 
            this.radioButtonAdministrativo.AutoSize = true;
            this.radioButtonAdministrativo.Location = new System.Drawing.Point(24, 49);
            this.radioButtonAdministrativo.Name = "radioButtonAdministrativo";
            this.radioButtonAdministrativo.Size = new System.Drawing.Size(90, 17);
            this.radioButtonAdministrativo.TabIndex = 6;
            this.radioButtonAdministrativo.TabStop = true;
            this.radioButtonAdministrativo.Text = "Administrativo";
            this.radioButtonAdministrativo.UseVisualStyleBackColor = true;
            // 
            // radioButtonDocente
            // 
            this.radioButtonDocente.AutoSize = true;
            this.radioButtonDocente.Location = new System.Drawing.Point(24, 26);
            this.radioButtonDocente.Name = "radioButtonDocente";
            this.radioButtonDocente.Size = new System.Drawing.Size(66, 17);
            this.radioButtonDocente.TabIndex = 5;
            this.radioButtonDocente.TabStop = true;
            this.radioButtonDocente.Text = "Docente";
            this.radioButtonDocente.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioButtonPUSPLQ);
            this.groupBox4.Controls.Add(this.radioButtonCENA);
            this.groupBox4.Controls.Add(this.radioButtonESALQ);
            this.groupBox4.Location = new System.Drawing.Point(16, 82);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(218, 109);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Unidade";
            // 
            // radioButtonPUSPLQ
            // 
            this.radioButtonPUSPLQ.AutoSize = true;
            this.radioButtonPUSPLQ.Location = new System.Drawing.Point(25, 72);
            this.radioButtonPUSPLQ.Name = "radioButtonPUSPLQ";
            this.radioButtonPUSPLQ.Size = new System.Drawing.Size(71, 17);
            this.radioButtonPUSPLQ.TabIndex = 7;
            this.radioButtonPUSPLQ.TabStop = true;
            this.radioButtonPUSPLQ.Text = "PUSP-LQ";
            this.radioButtonPUSPLQ.UseVisualStyleBackColor = true;
            // 
            // radioButtonCENA
            // 
            this.radioButtonCENA.AutoSize = true;
            this.radioButtonCENA.Location = new System.Drawing.Point(25, 49);
            this.radioButtonCENA.Name = "radioButtonCENA";
            this.radioButtonCENA.Size = new System.Drawing.Size(54, 17);
            this.radioButtonCENA.TabIndex = 6;
            this.radioButtonCENA.TabStop = true;
            this.radioButtonCENA.Text = "CENA";
            this.radioButtonCENA.UseVisualStyleBackColor = true;
            // 
            // radioButtonESALQ
            // 
            this.radioButtonESALQ.AutoSize = true;
            this.radioButtonESALQ.Location = new System.Drawing.Point(25, 26);
            this.radioButtonESALQ.Name = "radioButtonESALQ";
            this.radioButtonESALQ.Size = new System.Drawing.Size(60, 17);
            this.radioButtonESALQ.TabIndex = 5;
            this.radioButtonESALQ.TabStop = true;
            this.radioButtonESALQ.Text = "ESALQ";
            this.radioButtonESALQ.UseVisualStyleBackColor = true;
            // 
            // buttonImportar
            // 
            this.buttonImportar.Location = new System.Drawing.Point(360, 53);
            this.buttonImportar.Name = "buttonImportar";
            this.buttonImportar.Size = new System.Drawing.Size(75, 23);
            this.buttonImportar.TabIndex = 4;
            this.buttonImportar.Text = "Importar";
            this.buttonImportar.UseVisualStyleBackColor = true;
            this.buttonImportar.Click += new System.EventHandler(this.buttonImportar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Caminho:";
            // 
            // buttonOpen
            // 
            this.buttonOpen.Location = new System.Drawing.Point(360, 23);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(75, 23);
            this.buttonOpen.TabIndex = 0;
            this.buttonOpen.Text = "Abrir";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxPath
            // 
            this.textBoxPath.Location = new System.Drawing.Point(81, 25);
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.Size = new System.Drawing.Size(273, 20);
            this.textBoxPath.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(788, 197);
            this.panel1.TabIndex = 9;
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // enviarMensagemToolStripMenuItem
            // 
            this.enviarMensagemToolStripMenuItem.Name = "enviarMensagemToolStripMenuItem";
            this.enviarMensagemToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.enviarMensagemToolStripMenuItem.Text = "Enviar Mensagem";
            this.enviarMensagemToolStripMenuItem.Click += new System.EventHandler(this.enviarMensagemToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 477);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Importar Lista";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem iniciarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versãoToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxSaida;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxOrigem;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioButtonAdministrativo;
        private System.Windows.Forms.RadioButton radioButtonDocente;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButtonPUSPLQ;
        private System.Windows.Forms.RadioButton radioButtonCENA;
        private System.Windows.Forms.RadioButton radioButtonESALQ;
        private System.Windows.Forms.Button buttonImportar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.TextBox textBoxPath;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripMenuItem enviarMensagemToolStripMenuItem;
    }
}

